package com.twuc.webApp.yourTurn;

import com.twuc.webApp.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class Person {

    private Logger logger;

    public Logger getLogger() {
        return logger;
    }

    public Person(Logger logger) {
        this.logger = logger;
        logger.add("Person created");
    }
}
