package com.twuc.webApp;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.context.annotation.ScopedProxyMode;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE, proxyMode = ScopedProxyMode.TARGET_CLASS)
public class PrototypeDependentWithProxy {

    private Logger logger;

    public PrototypeDependentWithProxy(Logger logger) {
        this.logger = logger;
        logger.add("PrototypeDependentWithProxy");
    }

    void saySomething() {

    }
}
