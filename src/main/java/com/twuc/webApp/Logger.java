package com.twuc.webApp;

import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class Logger {
    public final static List<String> log = new ArrayList<>();

    public void add(String person_created) {
        log.add(person_created);
    }
}
