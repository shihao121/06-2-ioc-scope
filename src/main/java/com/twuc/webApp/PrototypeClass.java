package com.twuc.webApp;

import org.springframework.beans.factory.config.BeanDefinition;
import org.springframework.context.annotation.Scope;
import org.springframework.stereotype.Component;

@Component
@Scope(scopeName = BeanDefinition.SCOPE_PROTOTYPE)
public class PrototypeClass {

    private Logger logger;

    public Logger getLogger() {
        return logger;
    }

    public PrototypeClass(Logger logger) {
        this.logger = logger;
        logger.add("ProtoTypeClass created");
    }

    public void saySomething() {

    }
}
