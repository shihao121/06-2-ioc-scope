package com.twuc.webApp;

import org.springframework.stereotype.Component;

@Component
public class SingletonDependsOnPrototypeProxy {

    private PrototypeDependentWithProxy prototypeDependentWithProxy;
    private Logger logger;

    public Logger getLogger() {
        return logger;
    }

    public SingletonDependsOnPrototypeProxy(PrototypeDependentWithProxy prototypeDependentWithProxy,
                                            Logger logger) {
        this.logger = logger;
        this.prototypeDependentWithProxy = prototypeDependentWithProxy;
        logger.add("SingletonDependsOnPrototypeProxy");
    }

    public PrototypeDependentWithProxy getPrototypeDependentWithProxy() {
        return prototypeDependentWithProxy;
    }

    public void saySomething() {
        this.prototypeDependentWithProxy.saySomething();
    }
}
