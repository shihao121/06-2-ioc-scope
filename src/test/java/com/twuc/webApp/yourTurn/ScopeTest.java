package com.twuc.webApp.yourTurn;

import com.twuc.webApp.Logger;
import com.twuc.webApp.PrototypeClass;
import com.twuc.webApp.SingletonDependsOnPrototypeProxy;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class ScopeTest {

    private AnnotationConfigApplicationContext context;

    @BeforeEach
    private void createContext() {
        context = new AnnotationConfigApplicationContext("com.twuc.webApp");
    }

    @Test
    void should_crate_one_instance_when_get_abstract_and_subclass() {
        AbstractBaseClass abstratClass = context.getBean(AbstractBaseClass.class);
        DerivedClass derivedClass = context.getBean(DerivedClass.class);
        assertSame(abstratClass, derivedClass);
    }

    @Test
    void should_create_bean_when_application_context_load() {
        assertIterableEquals(Collections.singletonList("Person created"), Logger.log);
    }

    @Test
    void should_create_prototype_bean_when_get_bean() {
        context.getBean(PrototypeClass.class);
        assertTrue(Logger.log.contains("ProtoTypeClass created"));
    }

    @Test
    void should_create_new_instance_when_call_method() {
        SingletonDependsOnPrototypeProxy singleton = context.getBean(
                SingletonDependsOnPrototypeProxy.class);
        assertTrue(Logger.log.contains("SingletonDependsOnPrototypeProxy"));
        assertFalse(Logger.log.contains("PrototypeDependentWithProxy"));
        singleton.saySomething();
        assertTrue(Logger.log.contains("PrototypeDependentWithProxy"));
        singleton.saySomething();
        assertEquals(2, Logger.log.stream().filter(log -> {
            return log.equals("PrototypeDependentWithProxy");
        }).count());
    }

    @Test
    void should_not_create_dependence_instance_when_call_singleton_class_method() {
        SingletonDependsOnPrototypeProxy singleton = context.getBean(
                SingletonDependsOnPrototypeProxy.class);
        assertTrue(Logger.log.contains("SingletonDependsOnPrototypeProxy"));
        assertFalse(Logger.log.contains("PrototypeDependentWithProxy"));
        singleton.toString();
        assertEquals(0, Logger.log.stream().filter(log -> {
            return log.equals("PrototypeDependentWithProxy");
        }).count());

    }
}
